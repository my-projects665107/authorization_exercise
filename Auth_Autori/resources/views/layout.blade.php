<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your Laravel App</title>
</head>
<body>
<nav>
    <ul>
        @can(Auth::user())
            <li><a href="{{ route('auth.login') }}">Login</a></li>
            <li><a href="{{ route('auth.register') }}">Register</a></li>
        @endcan
        @auth
            <li><a href="{{ route('auth.logout') }}">Logout</a></li>
        @endauth
    </ul>
</nav>
@yield('content')

</body>
</html>
