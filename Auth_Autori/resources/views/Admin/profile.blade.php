<style>
    table, th, td {
        border: 1px solid black;
    }
</style>

@extends('layout')

@section('content')
    @auth
        @can('isAdmin')
            <h2>Admin Profile</h2>
            <p>Welcome, Admin! You can manage users here.</p>

            <h1>User List</h1>

            <table>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Settings</th>
                </tr>
                @foreach($users as $user)
                    <tr>
                        <th>{{$user->name}}</th>
                        <th>{{$user->email}}</th>
                        <th>{{$user->role}}</th>
                        <th><a href="{{ route('profile.edit', $user->id) }}">Edit User</a> /
                            <form method="Post" action="{{route('profile.destroy',$user->id)}}">
                                @csrf
                                @method('DELETE')
                                <button type="submit">Delete User</button>
                            </form>
                        </th>

                    </tr>
                @endforeach
            </table>
            <a href="{{ route('profile.create') }}">Create User</a>

        @else
            <h2>User Profile</h2>
            <p>Hello, {{ auth()->user()->name }}</p>
            <p><strong>Name:</strong> {{ auth()->user()->name }}</p>
            <p><strong>Email:</strong> {{ auth()->user()->email }}</p>
        @endif
    @endauth
@endsection
