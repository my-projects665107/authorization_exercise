@extends('layout')
@section('content')
    <h2>Edit User</h2>
    <a href="{{route('profile.index')}}">BACK</a>
    <form method="POST" action="{{ route('profile.update', $user->id) }}">
        @csrf
        @method('PUT')

        <div>
            <label for="u_name">Name:</label>
            <input type="text" name="u_name" id="u_name" value="{{ old('name', $user->name) }}" required>
        </div>

        <div>
            <label for="u_email">Email:</label>
            <input type="email" name="u_email" id="u_email" value="{{old('email', $user->email) }}" required>
        </div>


        <div>
            <label for="u_role">Role:</label>
            <select name="u_role" id="u_role">
                <option value="" selected disabled>Select Role</option>
                <option value="admin">Admin</option>
                <option value="user">User</option>
            </select>
        </div>

        <div>
            <button type="submit">Update User</button>
        </div>
    </form>
@endsection

