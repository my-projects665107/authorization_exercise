@extends('layout')
@section('content')
    <h2>Create User</h2>
    <a href="{{route('profile.index')}}">BACK</a>

    <form method="POST" action="{{ route('profile.store') }}">
        @csrf

        <div>
            <label for="name">Name:</label>
            <input type="text" name="name" id="name" required>
        </div>

        <div>
            <label for="email">Email:</label>
            <input type="email" name="email" id="email" required>
        </div>

        <div>
            <label for="password">Password:</label>
            <input type="password" name="password" id="password" required>
        </div>
        <div>
            <label for="role">Role:</label>
            <select name="role" id="role">
                <option value="" selected disabled>Select Role</option>
                <option value="admin">Admin</option>
                <option value="user">User</option>
            </select>
        </div>

        <div>
            <button type="submit">Create User</button>
        </div>
    </form>
@endsection

