<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('profile', compact('users'));
    }

    public function create()
    {
        return view('admin.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:3',
            'role' => 'required|',
        ]);
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role = $request->role;
        $user->save();

        return redirect()
            ->route('profile.index')
            ->with('success', 'User created successfully');
    }

    public function show()
    {
        return view('profile');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.edit', compact('user'));
    }


    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $request->validate([
            'u_name' => 'required',
            'u_email' => 'required|email|unique:users,email,',
            'u_role' => 'required',
        ]);
        $userUpdate = [
            'name' => $request->input('u_name'),
            'email' => $request->input('u_email'),
            'role' => $request->input('u_role'),

        ];

        $user->update($userUpdate);
        return redirect()->route('profile.index')->with('success', 'User updated successfully');
    }

    public function destroy($id)
    {
        $users = User::find($id);
        $users->delete();

        return redirect('profile')->with('success', 'User deleted successfully');
    }
}
